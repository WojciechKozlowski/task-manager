<?php

namespace App\Command;

use App\Repository\TasksRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\Table;

#[AsCommand(
    name: 'app:showOverdueTasks',
    description: 'This command shows overdue Tasks of the user',
)]
class ShowOverdueTasksCommand extends Command
{
        public function __construct(TasksRepository $tasksRepository, string $name = null)
        {
            $this->tasksRepository = $tasksRepository;

            parent::__construct($name);
        }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $task = $this->tasksRepository->getOverdueTasks();

        $table = new Table($output);
        $table
            ->setHeaders(['id', 'userid', 'name', 'description', 'deadline'])
            ->setRows($task)
        ;

        $table->render();

        return Command::SUCCESS;
    }
}
