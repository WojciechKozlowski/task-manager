<?php

namespace App\Controller;

use App\Repository\TasksRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;

class StatusController extends AbstractController
{
    private ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    #[Route('/status/{taskId}', name: 'app_change_status', methods: ['PUT'])]
    public function changeStatus(Request $request, int $taskId): Response
    {
        $tasksRepository = new TasksRepository($this->registry);

        $tasksRepository->setStatus(
            $taskId,
            boolval($request->request->all()['checked'])
        );

        return $this->json(null);
    }

}
