<?php

namespace App\Repository;

use App\Entity\Tasks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Tasks>
 *
 * @method Tasks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tasks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tasks[]    findAll()
 * @method Tasks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TasksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tasks::class);
    }

    public function add(Tasks $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Tasks $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Tasks[] Returns an array of Tasks objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    /**
//     * @throws NonUniqueResultException
//     */
//    public function findTasksByUser($user): ?Tasks
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.userId = :userId')
//            ->setParameter('userId', $user)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
    public function findTasksByUser($user): array
    {
        $userId = $user->getId();
        $qb = $this->createQueryBuilder('t');
        $qb->select();
        if (null !== $userId) {
            $qb
                ->where('t.user = :userId')
                ->setParameter('userId', $userId);

        }

        return $qb->getQuery()->execute();
    }

    public function setStatus(int $idTask, int $status): \Doctrine\DBAL\Result
    {
        $conn = $this->getEntityManager()->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        return $queryBuilder
            ->update('tasks', 't')
            ->set('t.status', ':status')
            ->where('t.id = :idTask')
            ->setParameter('idTask', $idTask)
            ->setParameter('status', $status)
            ->executeQuery();
    }

    public function getOverdueTasks()
    {
        $conn = $this->getEntityManager()->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        return $queryBuilder
            ->select('t.*')
            ->from('tasks', 't')
            ->where('t.deadline < :deadlineTimeDate')
            ->setParameter('deadlineTimeDate', (new \DateTime())->format('Y-m-d H:i:s'))
            ->executeQuery()
            ->fetchAllAssociative();
    }

    public function findTasksByDeadline($qb): QueryBuilder
    {
        $now = new \DateTime();

        $qb->andwhere('t.deadline > :deadline')->setParameter('deadline', $now->format('Y-m-d H:i:s'));

        return $qb;
    }

    public function findTasksByStatus($qbExceeded): QueryBuilder
    {
        $qbExceeded->andwhere('t.status = :status')->setParameter('status', 0);

        return $qbExceeded;
    }

    /**
     * @param $qb
     * @return QueryBuilder
     */
    public function findTasksByDeadlineExceeded($qb): QueryBuilder
    {
        $now = new \DateTime();

        $qb->andwhere('t.deadline < :deadline')->setParameter('deadline', $now->format('Y-m-d H:i:s'));

        return $qb;
    }

}
